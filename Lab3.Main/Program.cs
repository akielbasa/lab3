﻿using System;
using Lab3.Contract;


namespace Lab3
{
    public static class PolerowanieMixin
    {
            public static void Poleruj(this IDzialanie target)
            {
                Console.WriteLine("Wszystko się błyszczy");
            }

        }
    class Program
    {
        static void Main(string[] args)
        {
            Myjnia superMyjnia = new Myjnia();
            ((IZasilanie)superMyjnia).Zasil();
            ((IDzialanie)superMyjnia).Myj();
            ((IDzialanie)superMyjnia).Woskuj();
            ((IZasilanie)superMyjnia).Wylacz();

            Console.ReadKey();

        }
    }
}
