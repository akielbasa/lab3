﻿using System;
using Lab3.Contract;


namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IZasilanie);
        public static Type I2 = typeof(IDzialanie);
        public static Type I3 = typeof(IOplaty);

        public static Type Component = typeof(Myjnia);

        public static GetInstance GetInstanceOfI1 = new GetInstance(Lab3.Myjnia.getNewInstance);
        public static GetInstance GetInstanceOfI2 = new GetInstance(Lab3.Myjnia.getNewInstance);
        public static GetInstance GetInstanceOfI3 = new GetInstance(Lab3.Myjnia.getNewInstance);
        
        #endregion

        #region P2

        public static Type Mixin = typeof(PolerowanieMixin);
        public static Type MixinFor = typeof(IDzialanie);

        #endregion
    }
}
