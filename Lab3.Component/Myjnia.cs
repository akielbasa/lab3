﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public class Myjnia: IDzialanie, IOplaty, IZasilanie
    {
        private Dzialanie actionsManagment = new Dzialanie();
        private Oplaty  moneyManagement = new Oplaty();
        private Zasilanie powerManagement = new Zasilanie();
        public Myjnia()
        {
        }

        void IDzialanie.Myj()
        {
            actionsManagment.Myj();
        }

        void IDzialanie.Susz()
        {
            actionsManagment.Susz();
        }

        void IDzialanie.Woskuj()
        {
            actionsManagment.Woskuj();
        }

        void IOplaty.PobierzOplate()
        {
            moneyManagement.PobierzOplate();
        }

        void IOplaty.WydajReszte()
        {
            moneyManagement.WydajReszte();
        }

        void IZasilanie.Zasil()
        {
            powerManagement.Zasil();
        }

        void IZasilanie.Wylacz()
        {
            powerManagement.Wylacz();
        }

        public static object getNewInstance(object argument)
        {
            return new Myjnia();
        }
    }
}
